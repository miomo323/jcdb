#ifndef AGENT_FUNC
#define AGENT_FUNC
#include <jni.h>
#include <jvmti.h>
#include <cstdio>
#include <cstring>
#include <cstdlib>

#define TM_MAX_SIZE 16
typedef struct ThreadMapEntry {
	jthread thread;
	int num;
	int pcnt;
} Tme;

void JNICALL cbMethodEntry(jvmtiEnv* tienv, JNIEnv* env, jthread thread, jmethodID mid);
void JNICALL cbMonitorContendedEnter(jvmtiEnv* tienv, JNIEnv* env, jthread thread, jobject object);
void JNICALL cbMonitorContendedEntered(jvmtiEnv* tienv, JNIEnv* env, jthread thread, jobject object);
void JNICALL cbMonitorWait(jvmtiEnv* tienv, JNIEnv* env, jthread thread, jobject object, jlong timeout);
void JNICALL cbMonitorWaited(jvmtiEnv* tienv, JNIEnv* env, jthread thread, jobject object, jboolean timed_out);

char* split_signature(char* signature);
#endif