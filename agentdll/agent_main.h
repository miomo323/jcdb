#ifndef AGENT_MAIN
#define AGENT_MAIN
#include <jni.h>
#include <jvmti.h>
#include <cstdio>
#include <cstring>

#include "agent_func.h"

JNIEXPORT jint JNICALL
Agent_OnLoad(JavaVM* jvm, char* options, void* reserved);
#endif