#include "agent_func.h"

Tme tmes[TM_MAX_SIZE];
FILE* fw;

void JNICALL cbMethodEntry(jvmtiEnv* tienv, JNIEnv* env, jthread thread, jmethodID mid) {
	jclass clazz;
	char* csig;
	char* cname;
	char* mname;
	jvmtiThreadInfo thread_info;
	jint cnt;
	jvmtiError err;

	err = tienv->GetMethodDeclaringClass(mid, &clazz);
	err = tienv->GetClassSignature(clazz, &csig, 0);
	cname = split_signature(csig);
	err = tienv->GetMethodName(mid, &mname, NULL, NULL);
	err = tienv->GetThreadInfo(thread, &thread_info);

	if (std::strncmp(csig, "Ljava", 5) == 0 || std::strncmp(csig, "Ljdk", 4) == 0 || std::strncmp(csig, "Lsun", 4) == 0) {
		std::free((void*)cname);
		return;
	}

	jlong time;
	tienv->GetTime(&time);
	std::fprintf(fw, "[%lld] [method] [thread: %s] %s.%s(...)\n", time, thread_info.name, cname, mname);
	std::free((void*)cname);
}

void JNICALL cbMonitorContendedEnter(jvmtiEnv* tienv, JNIEnv* env, jthread thread, jobject object) {
	jvmtiThreadInfo thread_info;
	jvmtiError err;
	jclass clazz;
	char* csig;
	char* cname;

	err = tienv->GetThreadInfo(thread, &thread_info);
	clazz = env->GetObjectClass(object);
	err = tienv->GetClassSignature(clazz, &csig, NULL);
	cname = split_signature(csig);

	jlong time;
	tienv->GetTime(&time);
	std::fprintf(fw, "[%lld] [monitor] [thread: %s] TRY %s monitor!\n", time, thread_info.name, cname);
	std::free((void*)cname);
}

void JNICALL cbMonitorContendedEntered(jvmtiEnv* tienv, JNIEnv* env, jthread thread, jobject object) {
	jvmtiThreadInfo thread_info;
	jvmtiError err;
	jclass clazz;
	char* csig;
	char* cname;

	err = tienv->GetThreadInfo(thread, &thread_info);
	clazz = env->GetObjectClass(object);
	err = tienv->GetClassSignature(clazz, &csig, NULL);
	cname = split_signature(csig);

	jlong time;
	tienv->GetTime(&time);
	std::fprintf(fw, "[%lld] [monitor] [thread: %s] GET %s monitor!\n", time, thread_info.name, cname);
	std::free((void*)cname);
}

void JNICALL cbMonitorWait(jvmtiEnv* tienv, JNIEnv* env, jthread thread, jobject object, jlong timeout) {
	jvmtiThreadInfo thread_info;
	jvmtiError err;
	jclass clazz;
	char* csig;
	char* cname;

	err = tienv->GetThreadInfo(thread, &thread_info);
	clazz = env->GetObjectClass(object);
	err = tienv->GetClassSignature(clazz, &csig, NULL);
	cname = split_signature(csig);

	jlong time;
	tienv->GetTime(&time);
	std::fprintf(fw, "[%lld] [monitor] [thread: %s] WAIT on %s monitor!\n", time, thread_info.name, cname);
	std::free((void*)cname);
}

void JNICALL cbMonitorWaited(jvmtiEnv* tienv, JNIEnv* env, jthread thread, jobject object, jboolean timed_out) {
	jvmtiThreadInfo thread_info;
	jvmtiError err;
	jclass clazz;
	char* csig;
	char* cname;

	err = tienv->GetThreadInfo(thread, &thread_info);
	clazz = env->GetObjectClass(object);
	err = tienv->GetClassSignature(clazz, &csig, NULL);
	cname = split_signature(csig);

	jlong time;
	tienv->GetTime(&time);
	std::fprintf(fw, "[%lld] [monitor] [thread: %s] REGET %s monitor!\n", time, thread_info.name, cname);
	std::free((void*)cname);
}

char* split_signature(char* signature) {
	char* str_tmp = (char*)malloc(std::strlen(signature));
	int cnt = 0;

	for (int i = 1; signature[i] != ';'; i++) {
		if (signature[i] == '/') {
			str_tmp[cnt] = '.';
		}
		else {
			str_tmp[cnt] = signature[i];
		}

		cnt++;
	}
	str_tmp[cnt] = '\0';

	return str_tmp;
}