﻿#include "agent_main.h"

extern FILE* fw;

JNIEXPORT jint JNICALL
Agent_OnLoad(JavaVM* jvm, char* options, void* reserved) {
	jvmtiEnv* tienv;
	jvmtiError err;
	jint res;
	jvmtiCapabilities capas;
	jvmtiEventCallbacks cbs;

	std::printf("初始化agent！\n");

	res = jvm->GetEnv((void**)&tienv, JVMTI_VERSION_1);
	if (res != JNI_OK) {
		std::printf("无法获取JVMTI环境！\n");
		return JNI_ERR;
	}
	std::printf("成功获取JVMTI环境！\n");

	std::memset(&capas, 0, sizeof(capas));
	capas.can_generate_method_entry_events = 1;
	capas.can_generate_monitor_events = 1;
	//进入方法event
	err = tienv->AddCapabilities(&capas);
	if (err != JVMTI_ERROR_NONE) {
		std::printf("无法设置capabilies！\n");
		return JNI_ERR;
	}
	std::printf("成功设置capabilies！\n");

	std::memset(&cbs, 0, sizeof(cbs));
	cbs.MethodEntry = cbMethodEntry;
	cbs.MonitorContendedEnter = cbMonitorContendedEnter;
	cbs.MonitorContendedEntered = cbMonitorContendedEntered;
	cbs.MonitorWait = cbMonitorWait;
	cbs.MonitorWaited = cbMonitorWaited;
	err = tienv->SetEventCallbacks(&cbs, sizeof(cbs));
	if (err != JVMTI_ERROR_NONE) {
		std::printf("无法设置callbacks！\n");
		return JNI_ERR;
	}
	std::printf("成功设置callbacks！\n");

	err = tienv->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_METHOD_ENTRY, (jthread)NULL);
	if (err != JVMTI_ERROR_NONE) {
		std::printf("使能JVMTI_EVENT_METHOD_ENTRY失败！\n");
		return JNI_ERR;
	}
	err = tienv->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_MONITOR_CONTENDED_ENTER, (jthread)NULL);
	if (err != JVMTI_ERROR_NONE) {
		std::printf("使能JVMTI_EVENT_MONITOR_CONTENDED_ENTER失败！\n");
		return JNI_ERR;
	}
	err = tienv->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_MONITOR_CONTENDED_ENTERED, (jthread)NULL);
	if (err != JVMTI_ERROR_NONE) {
		std::printf("使能JVMTI_EVENT_MONITOR_CONTENDED_ENTERED失败！\n");
		return JNI_ERR;
	}
	err = tienv->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_MONITOR_WAIT, (jthread)NULL);
	if (err != JVMTI_ERROR_NONE) {
		std::printf("使能JVMTI_EVENT_MONITOR_WAIT失败！\n");
		return JNI_ERR;
	}
	err = tienv->SetEventNotificationMode(JVMTI_ENABLE, JVMTI_EVENT_MONITOR_WAITED, (jthread)NULL);
	if (err != JVMTI_ERROR_NONE) {
		std::printf("使能JVMTI_EVENT_MONITOR_WAITED失败！\n");
		return JNI_ERR;
	}
	std::printf("使能JVMTI_EVENT成功！\n");

	fw = fopen("log.txt", "w");

	return JNI_OK;
}