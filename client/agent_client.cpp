﻿#include "agent_client.h"

JavaVM* jvm;
JNIEnv* env;

int main() {
	JavaVMOption jvm_options[3];
	JavaVMInitArgs jvm_args;

	jvm_options[0].optionString = "-Djava.compiler=NONE";
	jvm_options[1].optionString = "-agentlib:agentdll";

	jvm_args.version = JNI_VERSION_1_2;
	jvm_args.nOptions = 3;
	jvm_args.options = jvm_options;
	jvm_args.ignoreUnrecognized = JNI_FALSE;

	char jvm_path[BUFSIZ] = { '\0' };
	std::strcat(jvm_path, std::getenv("JAVA_HOME"));
	std::strcat(jvm_path, "\\jre\\bin\\server\\jvm.dll");

	HINSTANCE hinstance = ::LoadLibrary(jvm_path);
	if (!hinstance) {
		std::printf("未找到jvm.dll！\n");
		return -1;
	}
	std::printf("找到jvm.dll！\n");

	char dependence_path[256] = {'\0'};
	std::strcat(dependence_path, "-Djava.class.path=");
	std::printf("输入classpath的相对路径：（“;”隔开不同的包）");
	std::scanf("%s", dependence_path + 18);
	jvm_options[2].optionString = dependence_path;
	

	typedef jint(WINAPI* FP_create_jvm)(JavaVM**, void**, void*);
	FP_create_jvm create_jvm = (FP_create_jvm)::GetProcAddress(hinstance, "JNI_CreateJavaVM");
	char res = (*create_jvm)(&jvm, (void**)&env, &jvm_args);
	if (res < 0) {
		std::printf("JVM创建失败！\n");
		return -1;
	}

	std::printf("JVM创建成功！\n");

	char main_path[256] = {0};
	std::printf("\n请输入主类的相对路径：（“/”分割文件夹）");
	std::scanf("%s", main_path);
	jclass Main_class = env->FindClass(main_path);
	if (Main_class == NULL) {
		std::printf("找不到Main类！\n");
		return -1;
	}

	jmethodID Main_main_ID = env->GetStaticMethodID(Main_class, "main", "([Ljava/lang/String;)V");
	env->CallStaticVoidMethod(Main_class, Main_main_ID, NULL);

	jvm->DestroyJavaVM();
	::FreeLibrary(hinstance);
	return 0;
}
